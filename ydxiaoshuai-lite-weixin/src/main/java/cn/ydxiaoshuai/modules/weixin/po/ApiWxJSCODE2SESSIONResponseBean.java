package cn.ydxiaoshuai.modules.weixin.po;


import lombok.Data;

/**
 * @Description wx.login接口返回的对象
 * @author 小帅丶
 * @className ApiWxJSCODE2SESSIONResponseBean
 * @Date 2019/8/5-16:07
 **/
@Data
public class ApiWxJSCODE2SESSIONResponseBean {
    //错误码
    private int errcode;
    //错误信息
    private String errmsg;
    //用户唯一标识
    private String openid;
    //会话密钥
    private String session_key;
    //用户在开放平台的唯一标识符，在满足 UnionID 下发条件的情况下会返回
    private String unionid;
}
