package cn.ydxiaoshuai.modules.facedynamic.service;

import cn.ydxiaoshuai.common.api.vo.api.FaceDriverVirtualResponseBean;
import cn.ydxiaoshuai.modules.facedynamic.entity.FaceDynamicTask;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 人脸动态任务
 * @Author: 小帅丶
 * @Date:   2021-05-13
 * @Version: V1.0
 */
public interface IFaceDynamicTaskService extends IService<FaceDynamicTask> {

    void saveByTaskId(String userId, FaceDriverVirtualResponseBean responseBean,Integer logType);

    FaceDynamicTask getOneByTaskId(String taskId);

    Integer getCountByUserId(String userId, Integer addFaceDrive);
}
