package cn.ydxiaoshuai.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.ydxiaoshuai.modules.system.entity.SysFillRule;

/**
 * @Description: 填值规则
 * @Author: 小帅丶
 * @Date: 2019-11-07
 * @Version: V1.0
 */
public interface SysFillRuleMapper extends BaseMapper<SysFillRule> {

}
