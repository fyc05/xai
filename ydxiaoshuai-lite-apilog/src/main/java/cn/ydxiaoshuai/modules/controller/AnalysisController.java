package cn.ydxiaoshuai.modules.controller;

import cn.ydxiaoshuai.common.api.vo.Result;
import cn.ydxiaoshuai.common.aspect.annotation.AutoLog;
import cn.ydxiaoshuai.common.system.base.controller.JeecgController;
import cn.ydxiaoshuai.modules.entity.LiteApiLog;
import cn.ydxiaoshuai.modules.service.ILiteApiLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Description: API日志记录表
 * @Author: 小帅丶
 * @Date: 2021年6月17日18:36:32
 * @Version: V1.0
 */
@Slf4j
@Api(tags = "简单统计")
@RestController
@RequestMapping("/log/analysis")
public class AnalysisController extends JeecgController<LiteApiLog, ILiteApiLogService> {
    @Autowired
    private ILiteApiLogService liteApiLogService;
    /**
     * 接口次数统计
     * @return
     */
    @AutoLog(value = "接口全部统计")
    @ApiOperation(value = "接口全部统计", notes = "接口全部统计")
    @GetMapping(value = "/allCount")
    public Result<?> allCount() {
        List<Map<String, Object>> allCount = liteApiLogService.getAllCount();
        return Result.OK("查询成功！",allCount);
    }


    /**
     * top 20 用户使用次数排行榜
     *
     * @return
     */
    @AutoLog(value = "用户使用次数排行榜")
    @ApiOperation(value="用户使用次数排行榜", notes="用户使用次数排行榜")
    @GetMapping(value = "/userRank")
    public Result<?> userRank() {
        List<Map<String, Object>> allCount = liteApiLogService.getUserRank();
        return Result.OK("查询成功！",allCount);
    }

    /**
     * 接口次数统计
     * @return
     */
    @AutoLog(value = "接口全部统计当日TOP10")
    @ApiOperation(value = "接口全部统计当日TOP10", notes = "接口全部统计当日TOP10")
    @GetMapping(value = "/allCountToday")
    public Result<?> allCountToday() {
        List<Map<String, Object>> allCount = liteApiLogService.getAllCountToDay();
        return Result.OK("查询成功！",allCount);
    }


    /**
     * top 20 用户使用次数排行榜
     *
     * @return
     */
    @AutoLog(value = "用户使用次数排行榜当日TOP10")
    @ApiOperation(value="用户使用次数排行榜当日TOP10", notes="用户使用次数排行榜当日TOP10")
    @GetMapping(value = "/userRankToday")
    public Result<?> userRankToday() {
        List<Map<String, Object>> allCount = liteApiLogService.getUserRankToDay();
        return Result.OK("查询成功！",allCount);
    }

}
