package cn.ydxiaoshuai.modules.service.impl;

import cn.ydxiaoshuai.modules.entity.LiteConfigNav;
import cn.ydxiaoshuai.modules.mapper.LiteConfigNavMapper;
import cn.ydxiaoshuai.modules.service.ILiteConfigNavService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 首页菜单配置表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
@Service
public class LiteConfigNavServiceImpl extends ServiceImpl<LiteConfigNavMapper, LiteConfigNav> implements ILiteConfigNavService {

}
