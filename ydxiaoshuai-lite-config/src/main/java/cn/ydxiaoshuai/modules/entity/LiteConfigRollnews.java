package cn.ydxiaoshuai.modules.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 首页滚动公告表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
@Data
@TableName("lite_config_rollnews")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="lite_config_rollnews对象", description="首页滚动公告表")
public class LiteConfigRollnews {
    
	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
	private String id;
	/**滚动文本*/
	@Excel(name = "滚动文本", width = 15)
    @ApiModelProperty(value = "滚动文本")
	private String scrollText;
	/**跳转URL*/
	@Excel(name = "跳转URL", width = 15)
    @ApiModelProperty(value = "跳转URL")
	private String h5Url;
	/**小程序跳转路由*/
	@Excel(name = "小程序跳转路由", width = 15)
    @ApiModelProperty(value = "小程序跳转路由")
	private String liteUrl;
	/**排序ID*/
	@Excel(name = "排序ID", width = 15)
    @ApiModelProperty(value = "排序ID")
	private Integer sortId;
	/**是否显示*/
	@Excel(name = "是否显示", width = 15)
    @ApiModelProperty(value = "是否显示")
	private Integer isShow;
	/**创建人*/
	@Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
	private String createBy;
	/**创建日期*/
	@Excel(name = "创建日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
	private Date createTime;
	/**更新人*/
	@Excel(name = "更新人", width = 15)
    @ApiModelProperty(value = "更新人")
	private String updateBy;
	/**更新日期*/
	@Excel(name = "更新日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
	private Date updateTime;
}
