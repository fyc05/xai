package cn.ydxiaoshuai.common.api.vo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className ImageClassifyResponseBean
 * @Description 图像识别红酒接口返回
 * @Date 2020/9/24-14:20
 **/
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImageClassifyOtherResponse extends BaiDuBase{

    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {
        /** 货币识别会返回此字段 */
        private String currencyName;
        /** 货币识别会返回此字段 */
        private String currencyCode;
        /** 货币识别会返回此字段 */
        private String currencyDenomination;
        /** 货币识别会返回此字段 */
        private String year;
        /** 地标识别会返回此字段 */
        private String landmark;
        /** 判断是否返回详细信息 */
        private int hasdetail;
        /** 红酒中文名 */
        private String wineNameCn;
        /** 红酒英文名 */
        private String wineNameEn;
        /** 国家中文名 */
        private String countryCn;
        /** 国家英文名 */
        private String countryEn;
        /** 产区中文名 */
        private String regionCn;
        /** 产区英文名 */
        private String regionEn;
        /** 子产区中文名 */
        private String subRegionCn;
        /** 子产区英文名 */
        private String subRegionEn;
        /** 酒庄中文名 */
        private String wineryCn;
        /** 酒庄英文名 */
        private String wineryEn;
        /** 酒类型 */
        private String classifyByColor;
        /** 糖分类型 */
        private String classifyBySugar;
        /** 色泽 */
        private String color;
        /** 葡萄品种 */
        private String grapeCn;
        /** 葡萄品种英文名 */
        private String grapeEn;
        /** 品尝温度 */
        private String tasteTemperature;
        /** 酒品描述 */
        private String description;
    }
}
