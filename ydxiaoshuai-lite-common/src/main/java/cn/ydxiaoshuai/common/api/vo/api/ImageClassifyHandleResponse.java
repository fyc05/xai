package cn.ydxiaoshuai.common.api.vo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className ImageClassifyResponseBean
 * @Description 图像处理接口返回
 * @Date 2020/9/25
 **/
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImageClassifyHandleResponse extends BaiDuBase{
    private String image;
}
